package com.hu.androidhtml;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {
	private TextView textView = null;
	private EditText editText = null;
	public ImageGetter imageGetter = null;
	private GridView gridView = null;
	private int imgRes[] = new int[] { R.drawable.face0, R.drawable.face1,
			R.drawable.face2, R.drawable.face3, R.drawable.face4,
			R.drawable.face5, R.drawable.face6, R.drawable.face7,
			R.drawable.face8, R.drawable.face9, R.drawable.face10,
			R.drawable.face11, R.drawable.face12, R.drawable.face13,
			R.drawable.face14, R.drawable.face15, R.drawable.face16,
			R.drawable.face18, R.drawable.face19, R.drawable.face20,
			R.drawable.face21, R.drawable.face22, R.drawable.face23,
			R.drawable.face24 };

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		textView = (TextView) findViewById(R.id.textview);
		editText = (EditText) findViewById(R.id.edttext);
		String string = "呵呵~~我是一个TextView";
		String string2 = "我和图图片一起存在";
		gridView = (GridView) findViewById(R.id.myGridView);
		gridView.setAdapter(new ImageAdapter2(this, imgRes));
		gridView.setVisibility(View.GONE);
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				editText.append(Html.fromHtml("<img src='" + imgRes[position]
						+ "'/>", imageGetter, null));
			}

		});

		imageGetter = new ImageGetter() {

			@Override
			public Drawable getDrawable(String source) {
				Drawable drawable = null;
				// 根据id从资源文件中获取图片对象
				int ID = Integer.parseInt(source);// 将字符串参数作为有符号的十进制整数进行解析

				drawable = getResources().getDrawable(ID);

				drawable.setBounds(0, 0, drawable.getIntrinsicHeight(),
						drawable.getIntrinsicWidth());// 设置图片占据的位置大小

				return drawable;
			}
		};

		textView.setText(Html.fromHtml("<h1><font color=#E61A6B>" + string
				+ "</font></h1>" + "<img src='" + R.drawable.f001 + "'/>"
				+ "<u><i><font color=#FF00FF>" + string2 + "</font></i></u>",
				imageGetter, null));

	}

	public void Button(View v) {

		gridView.setVisibility(View.VISIBLE);
	}

	private class ImageAdapter2 extends BaseAdapter {
		private Context context = null;
		private int[] imaRes = null;

		public ImageAdapter2(Context context, int[] imaRes) {
			// TODO Auto-generated constructor stub
			this.context = context;
			this.imaRes = imaRes;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return imaRes.length;// 获取资源的数量
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return imaRes[position];// 取得每一项的信息
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub

			return imaRes[position];// 取得指定项的ID

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView imageView = new ImageView(context);
			imageView.setImageResource(imaRes[position]);
			return imageView;
		}

	}

}
